package `in`.meatbazzar

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.fragment.app.FragmentManager
import kotlinx.android.synthetic.main.activity_sign_up.*

class SignUpActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sign_up)

        val adapter = AppointmentUpcomingListViewPagerAdapter(supportFragmentManager)
        adapter.addFragments(SignInFragment(), "SignIn")
        adapter.addFragments(SignUpFragment(), "SignUp")
        appointmentDetails_viewpager.adapter = adapter
        tabsLayout.setupWithViewPager(appointmentDetails_viewpager)


    }
    class AppointmentUpcomingListViewPagerAdapter(fragment: FragmentManager): androidx.fragment.app.FragmentPagerAdapter(fragment){
        private val fragmentList : MutableList<androidx.fragment.app.Fragment> = ArrayList()
        private val titleList : MutableList<String> = ArrayList()
        override fun getCount(): Int {
            return  fragmentList.size
        }

        override fun getItem(position: Int): androidx.fragment.app.Fragment {
            return fragmentList[position]
        }

        fun addFragments(fragment: androidx.fragment.app.Fragment, title: String){
            fragmentList.add(fragment)
            titleList.add(title)
        }

        override fun getPageTitle(position: Int): CharSequence? {
            return  titleList[position]
        }
    }
}
